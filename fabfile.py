# -*- coding: utf-8 -*-

from __future__ import with_statement

from app import settings

from fabric.api import abort, cd, env, lcd, local
from fabric.contrib.console import confirm

import os


env.hosts = getattr(settings, 'FABRIC_HOSTS', ['localhost'])
env.password = getattr(settings, 'FABRIC_PASSWORD', '')
env.colorize_errors = True


def _env_activate(env_path):

    local('source %s/bin/activate' % env_path, shell='/bin/bash')


def _env_collectstatic(env_path, clear=False):

    _env_manage(env_path, ('collectstatic --noinput %s' % ('--clear' if clear else '', )))


def _env_manage(env_path, command_and_options):

    local('%s/bin/python %s/sources/manage.py %s' %
        (env_path, env_path, command_and_options.strip(), ))


def _env_path(env_name=None):

    if not env_name:

        if settings.IS_DEV_ENV:
            env_name = 'dev'

        elif settings.IS_PROD_ENV:
            env_name = 'prod'

        else:
            raise ImproperlyConfigured('Impossible to detect current env')

    env_name = env_name.strip().upper()
    env_path = getattr(settings, 'ENV_DIR_' + env_name)

    if not env_path:
        raise ValueError('Invalid virtual env name: %s' % (env_name, ))

    return env_path


def _env_pip(env_path, command):

    return local('%s/bin/pip %s' % (env_path, command, ), capture=True)


def _env_restart(env_path):

    _env_collectstatic(env_path, clear=False)

    if 'fancy_cache' in settings.INSTALLED_APPS:
        _env_manage(env_path, 'fancy-urls --purge')

    if 'django_crontab' in settings.INSTALLED_APPS and len(getattr(settings, 'CRONJOBS', [])):
        _env_manage(env_path, 'crontab remove')
        _env_manage(env_path, 'crontab add')

    if env_path == settings.ENV_DIR_DEV:
        script_name = settings.ENV_NAME_DEV

    elif env_path == settings.ENV_DIR_PROD:
        script_name = settings.ENV_NAME_PROD

    local('~/init/%s restart' % (script_name, ))


def _env_sync_packages(env_path):

    with lcd(env_path):

        with lcd('sources'):

            get_packages = lambda s: set(filter(bool, [p.strip() for p in s.splitlines() if len(p) and p[0].isalpha()]))

            required_packages = get_packages(local('cat requirements.txt', capture=True))
            installed_packages = get_packages(_env_pip(env_path, 'freeze'))
            removed_packages = list(installed_packages - required_packages)
            added_packages = list(required_packages - installed_packages)

            for package in removed_packages:
                _env_pip(env_path, 'uninstall -y %s' % (package, ))

            for package in added_packages:
                _env_pip(env_path, 'install %s --no-cache-dir --no-deps' % (package, ))


def _env_to_git(env_name, commit_message):

    # local('pwd')

    # restart(env_name)

    env_path = _env_path(env_name)

    _env_activate(env_path)
    _env_restart(env_path)

    with lcd(env_path):

        with lcd('sources'):

            _env_pip(env_path, 'freeze > requirements.txt')

            local('git add -A')
            local('git status')

            if confirm('Continue deploy?'):
                local('git commit -m "%s"' % commit_message)
            else:
                local('git reset')
                abort('Abort deploy.')

            while True:
                try:
                    local('git push -u origin --all')
                    break
                except:
                    if not confirm('Retry?'):
                        abort('Abort git push.')


def _git_to_env(env_name):

    env_path = _env_path(env_name)

    _env_activate(env_path)

    with lcd(env_path):

        # Pull from .git, retry if wrong credentials
        with lcd('sources'):

            while True:
                try:
                    local('git pull origin master')
                    local('git clean -df')
                    break
                except:
                    if not confirm('Retry?'):
                        abort('Abort git pull.')

            local('git reset --hard origin/master')

        # Install and/or Uninstall pip packages checking diff
        _env_pip(env_path, 'install pip --upgrade')

    _env_sync_packages(env_path)
    _env_manage(env_path, 'migrate')
    _env_restart(env_path)


def collectstatic(env_name=None):

    env_path = _env_path(env_name)

    _env_activate(env_path)
    _env_collectstatic(env_path, clear=True)


def deploy(commit_message):

    _env_to_git('dev', commit_message)
    _git_to_env('prod')


def deploy_media():

    if confirm('Copy media from dev to prod?'):

        media_src = settings.MEDIA_ROOT
        media_dest = media_src.replace(
            settings.ENV_DIR_DEV, settings.ENV_DIR_PROD)

        # media_dest = os.path.abspath(os.path.join(os.path.dirname(media_dest), '..'))
        media_dest = os.path.dirname(os.path.dirname(media_dest))

        # print(media_src)
        # print(media_dest)

        local('cp -prfv %s %s' % (media_src, media_dest, ))


def deploy_db():

    if confirm('Copy db from dev to prod?'):

        print 'This is a TODO feature, meanwhile do manually:'
        print '- export db dev'
        print '- empty db prod'
        print '- import db dev into prod'


def deploy_and_publish():

    if confirm('Add prod domain and website proxy using the control panel (configure dns if needed). Have you done it?'):

        if confirm('Disable all non-admin users for the dev admin. Have you done it?'):

            if confirm('Add all allowed hosts to settings_dev.py and settings_prod.py. Have you done it?'):

                deploy('Published')
                deploy_media()
                deploy_db()

                if confirm('Duplicate entry in lighttpd/django.conf, replace dev with prod for socket url, media url and static url then swap dev/prod hosts in lighttpd/django.conf to test the prod website on the dev website. Have you done it?'):

                    reload_webserver()

                    if confirm('Set correct Site domain/name in Django admin. Have you done it?'):

                        if confirm('Enable non-admin users for the prod admin. Have you done it?'):

                            if confirm('If it\'s all ok swap-back dev/prod hosts in lighttpd/django.conf. Have you done it?'):

                                reload_webserver()

                                print 'WELL DONE! :)'


def git_to(env_name):

    _git_to_env(env_name)


def manage(env_name=None, command_and_options=''):

    env_path = _env_path(env_name)
    # print(env_path)

    _env_activate(env_path)
    _env_manage(env_path, command_and_options.strip())


def migrate(app_name=None):

    env_path = _env_path()

    _env_activate(env_path)

    if app_name:
        _env_manage(env_path, 'makemigrations %s' % (app_name, ))
        _env_manage(env_path, 'migrate %s' % (app_name, ))
    else:
        _env_manage(env_path, 'makemigrations')
        _env_manage(env_path, 'migrate')

    _env_restart(env_path)


def reload_webserver():
    """
    Reloads web server
    """
    local('~/init/nginx reload')


def remove_pyc_files(env_name=None):

    env_path = _env_path(env_name)

    with lcd(env_path):
        local('find . -name *.pyc -exec rm -rfv {} \;')

    _env_restart(env_path)


def restart(env_name=None):
    """
    Restarts application server
    """
    env_path = _env_path(env_name)

    _env_activate(env_path)
    _env_restart(env_path)


def runserver(env_name=None):

    env_path = _env_path(env_name)

    # manage(env_name, 'runserver %s:%s' % (settings.DEVSERVER_DEFAULT_ADDR, settings.DEVSERVER_DEFAULT_PORT, ))
    _env_manage(env_path, 'runserver')

