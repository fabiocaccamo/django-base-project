# -*- coding: utf-8 -*-

"""
app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
# from django.urls import reverse
from django.views.generic import TemplateView
# from django.views.generic.base import RedirectView

from navsy.sitemaps import PageSitemap


handler400 = 'website.views.handler400'
handler403 = 'website.views.handler403'
handler404 = 'website.views.handler404'
handler500 = 'website.views.handler500'


patterns_wrapper = lambda urls: i18n_patterns(urls) if settings.MULTILANGUAGE else [urls]

urlpatterns = []
sitemaps = {}

if settings.MULTILANGUAGE:

    urls = url(r'^i18n/', include('django.conf.urls.i18n'))
    urlpatterns += [urls]

    for language in settings.LANGUAGES:
        #sitemaps['modelname-%s' % (language[0], )] = ModelSitemap( language[0] )
        sitemaps['page-%s' % (language[0], )] = PageSitemap()
else:
    sitemaps['page'] = PageSitemap()
#     sitemaps['route'] = RouteSitemap({
#         '{{ route.view_name }}':{
#             'args': ['test'],
#             # 'kwargs': { 'slug':'test' },
#             # 'priority': 1.0, # 0.0 - 1.0
#             # 'changefreq': 'monthly', # daily, weekly, monthly, yearly
#         },
#     })

urls = url(r'^sitemap\.xml$', sitemap, { 'sitemaps':sitemaps }, name='django.contrib.sitemaps.views.sitemap')
urlpatterns += [urls]

urls = url(r'^robots\.txt$', TemplateView.as_view(template_name='website/robots.txt', content_type='text/plain'))
urlpatterns += [urls]

urls = url(r'^admin/', admin.site.urls)
urlpatterns += patterns_wrapper(urls)

# urls = url(r'^', include('website.urls'))
# urlpatterns += patterns_wrapper(urls)

urls = url(r'^', include('navsy.urls'))
urlpatterns += patterns_wrapper(urls)

if 'debug_toolbar' in settings.INSTALLED_APPS:
    try:
        import debug_toolbar
        urls = url(r'^__debug__/', include(debug_toolbar.urls))
        urlpatterns = [urls] + urlpatterns

    except ImportError:
        pass

if settings.DEBUG:

    if 'website' in settings.INSTALLED_APPS:

        urlpatterns = [
            url(r'^400$', TemplateView.as_view(template_name='website/400.html')),
            url(r'^403$', TemplateView.as_view(template_name='website/403.html')),
            url(r'^404$', TemplateView.as_view(template_name='website/404.html')),
            url(r'^500$', TemplateView.as_view(template_name='website/500.html')),
        ] + urlpatterns

    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

