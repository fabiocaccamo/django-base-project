# -*- coding: utf-8 -*-

from django import template
from django.template.defaultfilters import stringfilter
from django.utils.safestring import mark_safe

import random
import re


register = template.Library()


@register.filter
@stringfilter
def beautify_link(text, www=True):
    """
    Beautify a link by removing protocol, final slash and optionally the www.
    {{ obj.url|beautify_link:True }} -> http://www.google.com/ -> www.google.com
    {{ obj.url|beautify_link:False }} -> http://www.google.com/ -> google.com
    """
    text = text.replace('http://', '')
    text = text.replace('https://', '')
    if text[-1] == '/':
        text = text[0:-1]
    if not www and text.startswith('www.'):
        text = text[4:]
    return mark_safe(text)


@register.filter
@stringfilter
def iframe_embed(text):

    width = '100%'
    height = '100%'
    loading = 'lazy'

    text = text.strip()

    if ' width=' in text:
        text = re.sub(r'width=[\"\']{1}([\w]+)?[\"\']{1}', 'width="{}"'.format(width), text)
    else:
        text = text.replace('<iframe ', '<iframe width="{}" '.format(width))

    if ' height=' in text:
        text = re.sub(r'height=[\"\']{1}([\w]+)?[\"\']{1}', 'height="{}"'.format(height), text)
    else:
        text = text.replace('<iframe ', '<iframe height="{}" '.format(height))

    if ' loading=' in text:
        text = re.sub(r'loading=[\"\']{1}([\w]+)?[\"\']{1}', 'loading="{}"'.format(loading), text)
    else:
        text = text.replace('<iframe ', '<iframe loading="{}" '.format(loading))

    return mark_safe(text)


@register.filter
@stringfilter
def strip_newlines(text):
    """
    Removes all newline characters from a block of text.
    """
    # First normalize the newlines using Django's nifty utility
    normalized_text = normalize_newlines(text)
    # Then simply remove the newlines like so.
    return mark_safe(normalized_text.replace('\n', ''))


@register.filter
@stringfilter
def strip_whitespaces(text):
    return mark_safe(text.replace(' ', ''))


@register.filter
@stringfilter
def trim(text):
    return mark_safe(text.strip())


@register.filter
@stringfilter
def phone_url(text):
    text = re.sub(r'\+', '00', text)
    text = re.sub(r'\D', '', text)
    text = 'tel://%s' % (text, )
    return mark_safe(text)


class EncryptEmail(template.Node):

    def __init__(self, context_var):

        self.context_var = template.Variable(context_var)

    def render(self, context):

        email_address = self.context_var.resolve(context)
        character_set = '+-.0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz'
        char_list = list(character_set)
        random.shuffle(char_list)

        key = ''.join(char_list)

        cipher_text = ''
        random_id = 'e' + str(random.randrange(1, 999999999))

        for a in email_address:
            cipher_text += key[ character_set.find(a) ]

        script = 'var a="'+key+'";var b=a.split("").sort().join("");var c="'+cipher_text+'";var d="";'
        script += 'for(var e=0;e<c.length;e++)d+=b.charAt(a.indexOf(c.charAt(e)));'
        script += 'document.getElementById("'+random_id+'").innerHTML="<a href=\\"mailto:"+d+"\\">"+d+"</a>";'
        script = "eval(\""+ script.replace("\\","\\\\").replace('"','\\"') + "\");"
        script = '<script type="text/javascript">/*<![CDATA[*/'+script+'/*]]>*/</script>'

        return '<span id="'+ random_id + '">[javascript protected email address]</span>'+ script


def encrypt_email(parser, token):
    """
    Usage: {% encrypt_email user.email %}
    """
    tokens = token.contents.split()

    if len(tokens)!=2:
        raise template.TemplateSyntaxError("%r tag accept two argument" % tokens[0])

    return EncryptEmail(tokens[1])

register.tag('encrypt_email', encrypt_email)

