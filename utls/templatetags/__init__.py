# -*- coding: utf-8 -*-

import os

from slugify import slugify

from sorl.thumbnail.base import ThumbnailBackend, EXTENSIONS
from sorl.thumbnail.conf import settings
from sorl.thumbnail.helpers import tokey, serialize


#sorl-thumbnail seo backend
# THUMBNAIL_ENGINE = 'sorl.thumbnail.engines.convert_engine.Engine' #handles CMYK too
# THUMBNAIL_BACKEND = 'backends.ThumbnailSEOBackend'
# THUMBNAIL_UPSCALE = True
# THUMBNAIL_PROGRESSIVE = False
# THUMBNAIL_COLORSPACE = 'RGB'
# THUMBNAIL_PRESERVE_FORMAT = True
# THUMBNAIL_DEBUG = DEBUG


class ThumbnailSEOBackend(ThumbnailBackend):

    def _get_thumbnail_filename(self, source, geometry_string, options):

        file_key = tokey(source.key, geometry_string, serialize(options))

        file_basename, file_ext = os.path.splitext(os.path.basename(source.name))
        file_basename = slugify(file_basename)

        file_ext = EXTENSIONS[options['format']]

        file_name = '%s.%s' % (file_basename, file_ext, )
        file_path = os.path.join(settings.THUMBNAIL_PREFIX, file_key[:2], file_key[2:4], file_key, file_name)

        return file_path
        
        