# -*- coding: utf-8 -*-

from django.conf import settings
from django.contrib.admin.widgets import AdminFileWidget
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.db.models.fields.files import ImageFieldFile
from django.forms import Textarea
from django.utils.safestring import mark_safe
from django.utils.six import string_types

try:
    from sorl.thumbnail import get_thumbnail
except ImportError:
    pass


def use_sorl_thumbnail():
    return 'sorl.thumbnail' in settings.INSTALLED_APPS

def use_versatile_image_field():
    return 'versatileimagefield' in settings.INSTALLED_APPS


def AdminChangeListCopyURL(label='Copy URL', short_description=None, get_object_url=None, base_url=None):

    """
    Usage in ModelAdmin class:
    copy_url = AdminChangeListCopyURL(label='Copy URL', short_description='Copy URL')
    list_display = ('copy_url', ... )

    class Media:
        js = ['https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js']
    """

    def wrapper(self, obj):

        obj_url = None
        if callable(get_object_url):
            obj_url = get_object_url(obj)
        elif hasattr(obj, 'get_absolute_url'):
            obj_url = obj.get_absolute_url()
        else:
            obj_url = ''

        btn_url = obj_url
        btn_style = """
            padding:6px 12px;
            line-height: 25px;
            font-size: 10px;
            text-transform: uppercase;
            text-decoration: none;
            white-space: nowrap;
            """

        if not btn_url:
            btn_url = ''
            btn_style += """
                opacity: 0.2;
                pointer-events: none;
                """
        else:
            if base_url and not btn_url.startswith(base_url):
                btn_url = '{}{}'.format(base_url, btn_url)

        btn_js = """
            <script>
            if (typeof(ClipboardJS) !== 'undefined') {
                if(!window.clipboard){
                    window.clipboard = new ClipboardJS('.button--copy-url');
                }
            } else {
                console.error('Please add clipboard.js to your ModelAdmin Media class: \\nclass Media: \\n\\tjs = [\\'https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js\\']');
            }
            </script>
            """

        btn_html_format = """
            {}<a href="javascript:void(0);" class="button button--copy-url" data-clipboard-text="{}" style="{}">{} &crarr;</a>
            """

        btn_html = btn_html_format.format(btn_js, btn_url, btn_style, label)
        btn_html = mark_safe(btn_html)
        return btn_html

    wrapper.short_description = mark_safe(short_description or 'Copy URL')
    wrapper.allow_tags = True
    return wrapper


def AdminChangeListOpenURL(label='&nearr;', short_description=None, get_object_url=None):

    """
    Usage in ModelAdmin class:
    open_url = AdminChangeListOpenURL(label='Open URL', short_description='Open URL')
    list_display = ('open_url', ... )
    """

    def wrapper(self, obj):

        obj_url = None
        if callable(get_object_url):
            obj_url = get_object_url(obj)
        elif hasattr(obj, 'get_absolute_url'):
            obj_url = obj.get_absolute_url()
        else:
            obj_url = ''

        html_format = '<a href="{}" target="_blank">{}</a>'
        html_output = html_format.format(obj_url, label)
        html_output = mark_safe(html_output)
        return html_output

    wrapper.short_description = mark_safe(short_description or 'Open URL')
    wrapper.allow_tags = True
    return wrapper


def AdminChangeListImage(
        field_name,
        width=150,
        height=100,
        background_size='contain',
        background_color='rgba(0, 0, 0, 0.05)',
        extra_style='',
        placeholder='',
        short_description=None):

    """
    Usage in ModelAdmin class:
    image_display = AdminChangeListImageWidget('image', width=150, height=100, background_size='cover', background_color='#999999', short_description='Image')
    list_display = ('image_display', ... )
    """
    def wrapper(self, obj):

        field_obj = getattr(obj, field_name, None)
        image_url = ''
        image_html = ''

        if field_obj:
            field_cls = field_obj.__class__
            field_type = type(field_obj)

            if isinstance(field_obj, ImageFieldFile):

                image_url = field_obj.url

                # TODO: check if image_field is SORLFIeld or Versatile image field

                # try:
                #     max_size = '{}x{}'.format(width, height)
                #
                #     # image_obj = image_field
                #
                #     # if use_versatile_image_field():
                #     #     image_obj = image_field.thumbnail[max_size]
                #     # elif use_sorl_thumbnail():
                #     #     image_obj = get_thumbnail(image_field, max_size)
                #     # else:
                #     #     image_url = image_obj.url
                #     raise Exception
                #
                # except Exception:
                #     image_url = field_obj.url

            elif isinstance(field_obj, string_types):
                image_url = field_obj
            else:
                raise Exception('Unsupported field type: {}'.format(type(field_obj)))

        if placeholder and not image_url:
            if placeholder.startswith('http'):
                image_url = placeholder
            else:
                image_url = static(placeholder)

        image_style_format = """
            display: inline-block;
            width: {}px;
            height: {}px;
            background-color: {};
            background-size: {};
            background-repeat: no-repeat;
            background-position: center center;
            background-image: url(\'{}\');
            """
        image_style = image_style_format.format(
            width, height, background_color, background_size, image_url)

        image_html_format = '<span style="{} {}"></span>'
        image_html = image_html_format.format(image_style, (extra_style or ''))

        image_html = mark_safe(image_html)
        return image_html

    wrapper.short_description = mark_safe(short_description or field_name.title() or 'Image')
    wrapper.allow_tags = True
    return wrapper


class AdminImageWidget(AdminFileWidget):

    """
    Usage in ModelAdmin class:

    SIMPLE USAGE:

    formfield_overrides = {
        models.ImageField:{ 'widget':AdminImageWidget() },
    }

    ------------------------------------------

    COMPLEX USAGE:

    image_widget = AdminImageWidget(
        fields={
            'icon':{ 'width':100, 'height':100, 'css':'background-color:#CCCCCC; padding:10px;' },
            'avatar':{ 'circle':100 }
        }
    )
    formfield_overrides = {
        models.ImageField:{ 'widget':image_widget },
    }
    """
    def __init__(self, **kwargs):

        super(AdminImageWidget, self).__init__()

        self.fields_default_options = {
            '*':{
                'exclude_fields':None,
                'circle':None,
                'width':300,
                'height':200,
                'css':''
            }
        }

        self.fields_options = self.fields_default_options.copy()
        self.fields_options.update( kwargs.get('fields', {}) )

    def render(self, name, value, attrs = None):

        output = []

        image = value

        if image and getattr(image, 'url', None):

            try:
                image_width = image.width
                image_height = image.height
            except:
                image_width = 0
                image_height = 0

            field_name = name.split('-')[-1]
            #print('field name = %s' % field_name)

            #print(self.fields_default_options)
            #print(self.fields_options)
            field_options = self.fields_default_options.get('*', {}).copy()
            field_options.update( self.fields_options.get('*', {}) )
            field_options.update( self.fields_options.get(field_name, {}) )
            #print(field_options)
            #print('---')

            if not field_options:
                field_options = self.fields_default_options.get('*')

            exclude_fields = field_options.get('exclude_fields', None)

            if not exclude_fields or not field_name in exclude_fields:

                try:
                    thumb_circle = int(field_options.get('circle', 0))
                except:
                    thumb_circle = 0

                if thumb_circle:
                    thumb_width = thumb_circle
                    thumb_height = thumb_circle
                    css = 'border-radius: %spx; ' % (round(thumb_circle / 2.0))
                else:
                    try:
                        thumb_width = int(field_options['width'])
                    except:
                        thumb_width = 0

                    try:
                        thumb_height = int(field_options['height'])
                    except:
                        thumb_height = 0

                    css = ''

                css += field_options.get('css', '')

                css += 'max-width:%spx;' % (min(image_width or thumb_width, thumb_width or image_width), )
                css += 'max-height:%spx;' % (min(image_height or thumb_height, thumb_height or image_height), )

                thumb_geometry = ''
                thumb_geometry += '%s' % (thumb_width, ) if thumb_width else ''
                thumb_geometry += 'x%s' % (thumb_height, ) if thumb_height else ''

                data = (image.url, 'auto', 'auto', css, )

                if not image_width or image_width > thumb_width or not image_height or image_height > thumb_height:

                    if use_versatile_image_field():

                        thumb = image.thumbnail[thumb_geometry]

                        data = (thumb.url, thumb.width, thumb.height, css, )
                        #data = (thumb.url, 'auto', 'auto', css, )

                    elif use_sorl_thumbnail():

                        if thumb_circle:
                            thumb = get_thumbnail(value, thumb_geometry, crop='center')
                        else:
                            thumb = get_thumbnail(value, thumb_geometry)

                        data = (thumb.url, thumb.width, thumb.height, css, )

                    else:
                        #TODO: raise Exception('unsupported image library')
                        pass

                output.append('<p class="file-thumbnail">')
                output.append('<a href="%s" target="_blank">' % (value.url, ))
                output.append('<img src="%s" width="%s" height="%s" style="%s">' % data)
                output.append('</a>')
                output.append('</p>')

        output.append(super(AdminFileWidget, self).render(name, value, attrs))

        return mark_safe(u''.join(output))


class SmallTextareaWidget(Textarea):

    def __init__(self, rows=3, cols=50):
        super(SmallTextareaWidget, self).__init__(
            attrs={ 'rows':rows, 'cols':cols })
