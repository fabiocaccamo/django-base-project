# -*- coding: utf-8 -*-

from django.apps import apps
from django.core.management.base import BaseCommand, CommandError
from django.db import connection


class Command(BaseCommand):
    
    help = 'Clean auto-increment for all tables.'
    
    def handle(self, **options):
        
        cursor = connection.cursor()
        
        for model in apps.get_models():
            
            table_name = model._meta.db_table
            print('Clean auto-increment for table: %s' % (table_name, ))
            sql = 'ALTER TABLE %s AUTO_INCREMENT = 1' % (table_name, )
            cursor.execute(sql)
            
        return
        
        