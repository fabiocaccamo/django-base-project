# -*- coding: utf-8 -*-

from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    
    help = 'Clean static files.'
    
    def handle(self, **options):
        
        call_command('collectstatic', '--noinput', '--clear' )
        
        return
        
        