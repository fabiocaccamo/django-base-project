# -*- coding: utf-8 -*-

from django.core.cache import cache
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    
    help = 'Clean cache.'
    
    def handle(self, **options):
        
        cache.clear()
        
        return
        
        