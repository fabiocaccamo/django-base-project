# -*- coding: utf-8 -*-

from django.contrib.auth.models import Permission
from django.core.management.base import BaseCommand, CommandError
from django.core.management import call_command


class Command(BaseCommand):
    
    help = 'Clean permissions.'
    
    def handle(self, **options):
        
        for obj in Permission.objects.all():
            obj.delete()
        
        call_command('migrate')
        
        return
        
        