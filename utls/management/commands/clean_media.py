# -*- coding: utf-8 -*-

import django

from django.conf import settings
from django.core.management.base import BaseCommand
from django.apps import apps
from django.db import models

import os


class Command(BaseCommand):

    help = 'Clean media by deleting those files which are no more ' \
           'referenced by any FileField.'

    def add_arguments(self, parser):

        parser.add_argument(
            '--noinput',
            action='store_true',
            default=False,
            help='Clean media by deleting those files which are no more '
            'referenced by any FileField without asking for confirmation.')

    def get_models_files(self):

        models_files = []

        for model in apps.get_models():

            # print(model)

            model_fields = model._meta.get_all_field_names() \
                if django.VERSION < (1, 10) else model._meta.get_fields()

            model_file_fields = [
                field.name for field in model_fields if isinstance(
                    field, models.FileField)]

            if not len(model_file_fields):
                continue

            # print(u'%s: %s' % (model.__name__, model_file_fields, ))

            model_files = []
            model_objects = list(model.objects.all())

            for obj in model_objects:
                for field in model_file_fields:
                    file = getattr(obj, field, None)
                    if file:
                        # print(file.path)
                        models_files.append(file.path)

        return models_files

    def get_media_files(self):

        exclude_paths = getattr(settings, 'CLEAN_MEDIA_EXCLUDE_PATHS', ('cache', ))
        exclude_roots = [os.path.normpath(
            settings.MEDIA_ROOT + '/' + path) for path in exclude_paths]

        media_files = []

        for root, dirs, files in os.walk(settings.MEDIA_ROOT):

            exclude_root = False

            for excluded_root in exclude_roots:
                if root.find(excluded_root) == 0:
                    exclude_root = True
                    break

            if exclude_root:
                continue

            for file in files:
                file_path = os.path.join(root, file)
                # print(file_path)
                media_files.append(file_path)

        return media_files

    def handle(self, *args, **options):

        models_files = self.get_models_files()
        media_files = self.get_media_files()
        unref_files = [
            file for file in media_files if not file in models_files]

        files = unref_files
        for file in files:
            print('founded unreferenced file: %s' % (file))

        files_count = len(files)
        print('founded %s unreferenced file%s' %
              (files_count, 's' if files_count else '', ))

        if files_count:
            remove_files_confirm = 'remove all unreferenced files? (y/N) '
            remove_files = options['noinput'] or raw_input(
                remove_files_confirm).lower().find('y') == 0

            if remove_files:
                for file in files:
                    try:
                        os.remove(file)
                        print('removed unreferenced file: %s' % (file))
                    except:
                        continue

        return

