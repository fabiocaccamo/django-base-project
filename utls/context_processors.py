# -*- coding: utf-8 -*-

from django.conf import settings
from django.contrib.sites.models import Site


def info(request):

    debug = settings.DEBUG
    current_site = Site.objects.get_current()
    secure = request.is_secure()
    protocol = 'https' if secure else 'http'
    domain = current_site.domain
    host = domain if domain.startswith('http') else '%s://%s' % (protocol, domain, )

    return {
        'debug': debug,
        'secure': secure,
        'protocol': protocol,
        'domain': domain,
        'host': host,
    }