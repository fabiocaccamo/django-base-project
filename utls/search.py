# -*- coding: utf-8 -*-

from django.db.models import Q

from slugify import slugify
# import re


# def get_query_terms( query_string, split_terms = True, min_term_length = 3, trim_term_last_char_over_length = 5 ):
#
#     """
#     Splits the query string in invidual keywords, getting rid of unecessary spaces
#     and grouping quoted words together.
#     Example:
#
#     >>> normalize_query('  some random  words "with   quotes  " and   spaces')
#     ['some', 'random', 'words', 'with quotes', 'and', 'spaces']
#     """
#
#     findterms_re = re.compile(r'"([^"]+)"|(\S+)')
#     normspace_re = re.compile(r'\s{2,}')
#
#     terms = []
#     terms_raw = findterms_re.findall(query_string) if split_terms else [(query_string, )]
#
#     for term_raw in terms_raw:
#
#         term = normspace_re.sub(' ', (term_raw[0] or term_raw[1]).strip())
#         term_length = len(term)
#
#         if term_length >= min_term_length:
#
#             if term_length > trim_term_last_char_over_length and trim_term_last_char_over_length > min_term_length:
#                 term = term[:-1]
#
#             terms.append(term.lower())
#
#     return terms


# def get_query(query_string, search_fields, split_terms=True, min_term_length=3, trim_term_last_char_over_length=5):
def get_query_filters(query_string, fields, terms_min_length=3):

    """
    Returns query filters, that is a combination of Q objects.
    The returned filters aims to search all search terms (AND)
    within a model by testing the given search fields (OR).
    """

    query = None # Query to search for every search term
    lookups = [
        '__exact', '__iexact',
        '__contains', '__icontains',
        '__in', '__gt', '__gte', '__lt', '__lte',
        '__startswith', '__istartswith',
        '__endswith', '__iendswith',
        '__range',
        '__date', '__year', '__iso_year', '__month', '__day', '__week', '__week_day', '__quarter',
        '__time', '__hour', '__minute', '__second',
        '__isnull',
        '__regex', '__iregex',
    ]

    # terms = get_query_terms(query_string, split_terms, min_term_length, trim_term_last_char_over_length)
    terms = slugify(query_string).split('-')

    for term in terms:

        if len(term) < terms_min_length:
            continue

        term_query = None

        for field_name in fields:

            field_name_has_lookup = False
            for lookup in lookups:
                if not field_name_has_lookup and field_name.endswith(lookup):
                    field_name_has_lookup = True
                    break

            if field_name_has_lookup:
                field_name_with_lookup = field_name
            else:
                field_name_with_lookup = '{}__icontains'.format(field_name)

            field_term_query = Q(**{ field_name_with_lookup: term, })

            if term_query is None:
                term_query = field_term_query
            else:
                term_query = term_query | field_term_query

        if query is None:
            query = term_query
        else:
            query = query & term_query

    return query

