# -*- coding: utf-8 -*-

from django.conf import settings
from django.core.mail import EmailMessage
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string


def send_mail(
            email_from,
            email_to,
            subject,
            message_text=None,
            message_text_template=None,
            message_html=None,
            message_html_template=None,
            message_data=None,
            attachments=None):

    item_list = lambda i: list(i) if isinstance(i, (list, tuple, )) else [i]
    from_to = {
        'from_email': (email_from or settings.DEFAULT_FROM_EMAIL),
        'to': item_list(email_to),
    }

    message_data = message_data or {}

    if message_html_template:
        if message_html:
           raise ValueError('message_html value will be replaced by message_html_template rendered string.')
        message_html = render_to_string(message_html_template, message_data)

    if message_text_template:
        if message_text:
           raise ValueError('message_text value will be replaced by message_text_template rendered string.')
        message_text = render_to_string(message_text_template, message_data)

    if message_html and message_text:
        message = EmailMultiAlternatives(subject, message_text, **from_to)
        message.attach_alternative(message_html, 'text/html')
    elif message_html:
        message = EmailMessage(subject, message_html, **from_to)
        message.content_subtype = 'html'
    elif message_text:
        message = EmailMessage(subject, message_text, **from_to)
    else:
        message = EmailMessage(subject, '', **from_to)

    attachments = item_list(attachments or [])
    for file in attachments:
        message.attach_file(file.path)

    message.send()


# def mail_admins(
#         subject, data=None, attachments=None,
#         message_html=None, message_html_template=None,
#         message_text=None, message_text_template=None):
#     # TODO
#     pass
#
#
# def mail_managers(
#         subject, data=None, attachments=None,
#         message_html=None, message_html_template=None,
#         message_text=None, message_text_template=None):
#     # TODO
#     pass

