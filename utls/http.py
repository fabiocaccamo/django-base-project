# -*- coding: utf-8 -*-

from django.core.files.base import ContentFile
from django.db.models import FileField
from django.http import HttpResponse#, StreamingHttpResponse
from django.utils.six import b

import base64
import csv
import io
import os
import mimetypes
import StringIO
import xlwt # excel format support -> pip install xlwt
import zipfile


def FileResponse(name=None, extension=None, file=None, data=None, content_type=None, download=True):

    file_name = ''
    file_extension = ''

    if file:
        file_path = getattr(file, 'path', None)
        if file_path:
            file_name, file_extension = os.path.splitext(file_path)

    file_name = name or file_name.lower() or ''
    file_name = file_name.strip()

    file_extension = extension or file_extension.lower() or ''
    file_extension = file_extension.strip()

    if file_extension and not file_extension.startswith('.'):
        file_extension = '.' + file_extension

    if file_name and not file_name.lower().endswith(file_extension):
        file_name = file_name + file_extension
        file_name = file_name.replace('..', '.')

    if content_type:
        file_content_type = content_type.strip()
    else:
        file_content_types = mimetypes.guess_type(file_name, strict=True)
        file_content_type = file_content_types[0]

    if file:
        response = HttpResponse(file, content_type=file_content_type)
    else:
        response = HttpResponse(content_type=file_content_type)

    response_content_disposition = 'attachment; ' if download else ''
    response_content_disposition += 'filename="%s"' % (file_name, )
    response['Content-Disposition'] = response_content_disposition

    if data:

        try:
            str_base_class = basestring
        except NameError:
            str_base_class = str

        if isinstance(data, str_base_class):
            # maybe base64 encoded data, try to decode it
            try:
                data_decoded = base64.decodestring(data)
                data = data_decoded
            except:
                pass

        response.write(data)

    return response


def CSVFileResponse(name=None, file=None, columns=None, rows=None, delimiter=';', download=True):

    response = FileResponse(name=name, extension='csv', file=file, data=None, content_type='text/csv', download=download)
    if columns or rows:
        writer = csv.writer(response, delimiter=delimiter, quoting=csv.QUOTE_ALL)
        # python 2 only
        def fix_encoding(s):
            if isinstance(s, unicode):
                return s.encode('utf-8')
            else:
                return s
        if columns:
            writer.writerow([fix_encoding(item) for item in columns])
        if rows:
            for row in rows:
                writer.writerow([fix_encoding(item) for item in row])
    return response


def XLSFileResponse(name=None, file=None, columns=None, rows=None, download=True):

    # https://www.pythoncircle.com/post/190/how-to-download-data-as-csv-and-excel-file-in-django/
    response = FileResponse(name=name, extension='xls', file=file, data=None, content_type='application/ms-excel', download=download)

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet(name)

    row_index = 0

    if columns:
        font_style = xlwt.XFStyle()
        font_style.font.bold = True
        for col_index in range(len(columns)):
            ws.write(row_index, col_index, columns[col_index], font_style)
        row_index += 1

    if rows:
        font_style = xlwt.XFStyle()
        for row in rows:
            for col_index in range(len(row)):
                ws.write(row_index, col_index, row[col_index], font_style)
            row_index += 1

	wb.save(response)
	return response


def PDFFileResponse(name=None, file=None, data=None, download=True):

    response = FileResponse(name=name, extension='pdf', file=file, data=data, content_type='application/pdf', download=download)
    return response


def ZIPFileResponse(name=None, file=None, data=None, download=True):

    response = FileResponse(name=name, extension='zip', file=file, data=data, content_type='application/zip', download=download)
    return response


def DynamicZIPFileResponse(name=None, files=[], path=''):

    temp_file = ContentFile(b(''), name=name)

    with zipfile.ZipFile(temp_file, mode='w', compression=zipfile.ZIP_DEFLATED) as zip_file:

        for file in files:
            file_path = file.name
            file_dir, file_name = os.path.split(file_path)
            file_str = file.read()

            zip_file.writestr(
                file_path if path == None else os.path.join(path, file_name),
                file_str)

    file_size = temp_file.tell()
    temp_file.seek(0)

    response = ZIPFileResponse(name=name, file=temp_file, download=True)
    response['Content-Length'] = file_size
    return response

