# -*- coding: utf-8 -*-

from django.db import models


class CustomManager(models.Manager):

    use_for_related_fields = True

    def __init__(self, *args, **kwargs):

        self._filter = kwargs.pop('filter', {})
        self._exclude = kwargs.pop('exclude', {})
        self._select_related = kwargs.pop('select_related', [])
        self._prefetch_related = kwargs.pop('prefetch_related', [])

        super(CustomManager, self).__init__(*args, **kwargs)

    def get_queryset(self):
        qs = super(CustomManager, self).get_queryset()
        if self._filter:
            qs = qs.filter(**self._filter)
        if self._exclude:
            qs = qs.exclude(**self._exclude)
        if self._select_related:
            qs = qs.select_related(*self._select_related)
        if self._prefetch_related:
            qs = qs.prefetch_related(*self._prefetch_related)
        return qs
