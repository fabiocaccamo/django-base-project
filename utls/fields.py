# -*- coding: utf-8 -*-

import django

from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models.fields.related import ReverseOneToOneDescriptor


class ReverseOneToOneDescriptorOrNone(ReverseOneToOneDescriptor):

    def __get__(self, instance, cls=None):
        try:
            return super(ReverseOneToOneDescriptorOrNone, self).__get__(instance, cls)
        except ObjectDoesNotExist:
            return None


class OneToOneOrNoneField(models.OneToOneField):
    """
    A OneToOneField that returns None if the related object doesn't exist
    https://stackoverflow.com/questions/3955093/django-return-none-from-onetoonefield-if-related-object-doesnt-exist
    """
    related_accessor_class = ReverseOneToOneDescriptorOrNone


class CharNullField(models.CharField):  # subclass the CharField
    description = "CharField that stores NULL but returns ''"
    __metaclass__ = models.SubfieldBase  # this ensures to_python will be called

    def to_python(self, value):
        # this is the value right out of the db, or an instance
        # if an instance, just return the instance
        if isinstance(value, models.CharField):
            return value
        if value is None:  # if the db has a NULL (None in Python)
            return ''      # convert it into an empty string
        else:
            return value   # otherwise, just return the value

    def get_prep_value(self, value):  # catches value right before sending to db
        if value == '':
            # if Django tries to save an empty string, send the db None (NULL)
            return None
        else:
            # otherwise, just pass the value
            return value


try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO

from django.core.files.base import ContentFile
from django.db.models.fields.files import ImageField, ImageFieldFile

from PIL import Image


def _update_ext(filename, new_ext):
    parts = filename.split('.')
    parts[-1] = new_ext
    return '.'.join(parts)


class ResizedImageFieldFile(ImageFieldFile):

    def save(self, name, content, save=True):

        new_content = StringIO()
        content.file.seek(0)

        img = Image.open(content.file)
        img.thumbnail((
                self.field.max_width,
                self.field.max_height,
            ), Image.ANTIALIAS)
        img.save(new_content, format=self.field.format)

        new_content = ContentFile(new_content.getvalue())
        new_name = _update_ext(name, self.field.format.lower())

        super(ResizedImageFieldFile, self).save(new_name, new_content, save)


class ResizedImageField(ImageField):

    """
    Usage:
    image = ResizedImageField(upload_to='images/', max_width=1000, max_height=1000)
    """

    attr_class = ResizedImageFieldFile

    def __init__(self, max_width=100, max_height=100, format='PNG', *args, **kwargs):

        self.max_width = max_width
        self.max_height = max_height
        self.format = format

        super(ResizedImageField, self).__init__(*args, **kwargs)

