# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib import admin
from django.conf import settings
#from django.contrib.admin import DateFieldListFilter
from django.db import models
#from django.db.models import Q
from django.forms import ModelForm, Textarea, TextInput

from solo.admin import SingletonModelAdmin

from sorl.thumbnail import get_thumbnail
# from sorl.thumbnail.admin import AdminImageMixin

from modeltranslation.admin import TranslationAdmin, TabbedTranslationAdmin, TranslationStackedInline, TranslationTabularInline

from utls.widgets import AdminImageWidget

from website.models import *


'''
def custom_action(modeladmin, request, queryset):
    pass
    
custom_action.short_description = 'Custom action'



class CustomModelInline( admin.StackedInline ):
    
    model = CustomModel
    extra = 0


    
# class CustomModelAdmin( AdminImageMixin, TabbedTranslationAdmin ):
# class CustomModelAdmin( admin.ModelAdmin ):
class CustomModelAdmin( SingletonModelAdmin ):
    
    fieldsets = (
        ('SEO', {
            'classes': ('collapse', ),
            'fields': ('seo_title', 'seo_meta_description', 'seo_meta_keywords', )
        }),
        (None, {
            'classes': ('wide', ),
            'fields': ('name', 'text', )
        }),
        
    def image_preview(self, obj):
        
        if obj.thumb:
            return '<img style="padding:5px;" src="%s" height="%s"/>' % (obj.thumb.url, 50, )

        return ''
    
    image_preview.short_description = 'Anteprima'
    image_preview.allow_tags = True
    
    actions = []
    list_filter = ('',)
    search_fields = ('',)
    list_display = [""]
    list_display_links = ('',)
    readonly_fields = ('',)
    filter_horizontal = ('',)
    inlines = [ CustomModel ]
    
    formfield_overrides = {
        models.CharField:{ 'widget':Textarea( attrs={ 'rows':3, 'cols':45 } ) },
        models.ImageField:{ 'widget':AdminImageWidget( fields={ 'icon':{ 'width':100, 'height':100, 'css':'background-color:#CCCCCC; padding:10px;' }, 'avatar':{ 'circle':100 } } ) }, 
    }
    
admin.site.register(CustomModel, CustomModelAdmin)
'''

'''
# class SiteSettingsAdmin( SingletonModelAdmin, TabbedTranslationAdmin ):
class SiteSettingsAdmin( SingletonModelAdmin ):
    
    fieldsets = (
        
        ('Info', {
            'classes': ('wide', ),
            'fields': ('company', 'piva', 'copyright', )
        }),
        
        ('Contacts', {
            'classes': ('wide', ),
            'fields': ('phone', 'fax', 'email', 'skype', )
        }),
        
        ('Location', {
            'classes': ('wide', ),
            'fields': ('address', 'latitude', 'longitude', 'maps_url', )
        }),
        
        ('Google Analitycs', {
            'classes': ('wide', ),
            'fields': ('google_analitycs_code', 'google_analitycs_id', )
        }),
        
        ('Socials', {
            'classes': ('wide', ),
            'fields': ('facebook_url', 'twitter_url', 'linkedin_url', 'youtube_url', 'vimeo_url', 'googleplus_url', 'tumblr_url', 'instagram_url', 'pinterest_url', )
        }),
        
        ('Privacy', {
            'classes': ('wide', ),
            'fields': ('privacy_title', 'privacy_text', )
        }),
    )
    
    save_on_top = True
    
admin.site.register( SiteSettings, SiteSettingsAdmin )



# class SiteErrorsAdmin( SingletonModelAdmin, TabbedTranslationAdmin ):
class SiteErrorsAdmin( SingletonModelAdmin ):
    
    fieldsets = (
        ('400', {
            'classes': ('extrawide', ),
            'fields': ('title_400', 'subtitle_400', )
        }),
        ('403', {
            'classes': ('extrawide', ),
            'fields': ('title_403', 'subtitle_403', )
        }),
        ('404', {
            'classes': ('extrawide', ),
            'fields': ('title_404', 'subtitle_404', )
        }),
        ('500', {
            'classes': ('extrawide', ),
            'fields': ('title_500', 'subtitle_500', )
        }),
    )
    
    save_on_top = True
    
admin.site.register( SiteErrors, SiteErrorsAdmin )
'''

