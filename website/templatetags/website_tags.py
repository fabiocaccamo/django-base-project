# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django import template
from django.template.defaultfilters import stringfilter

from website.models import *


register = template.Library()


"""
@register.inclusion_tag('templatetags/xxx.html', takes_context=True)
def xxx(context):
    zzz = context['zzz']
    context['yyy'] = yyy
    return context
"""

"""
@register.assignment_tag( takes_context = True )
def load_mymodels( context ):
    mymodels = MyModel.objects.all()
    return mymodels
"""
