# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf import settings
# from django.contrib.auth.models import User
# from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.db import models
# from django.db.models.signals import post_save
# from django.forms import ValidationError
# from django.template.defaultfilters import slugify

# from colorfield.fields import ColorField
from autoslug import AutoSlugField
from ckeditor.fields import RichTextField as HTMLField
from navsy.urls import reverse
from solo.models import SingletonModel
from sorl.thumbnail import get_thumbnail, ImageField
# from utls.fields import *
from website.managers import *

# import datetime
# import uuid

"""
models.AutoField
models.BigIntegerField
models.BooleanField
models.CharField
models.CommaSeparatedIntegerField
models.DateField
models.DateTimeField
models.DecimalField
models.EmailField
models.FileField     
models.FilePathField
models.FloatField
models.IntegerField
models.IPAddressField
models.GenericIPAddressField
models.NullBooleanField
models.PositiveIntegerField
models.PositiveSmallIntegerField
models.SlugField
models.SmallIntegerField
models.TextField
models.TimeField
models.URLField
"""


"""
class MyModel(models.Model):
    
    name = models.CharField( max_length = 50, blank = True, verbose_name = 'Name' )
    slug = AutoSlugField( populate_from = 'name', always_update = True, editable = False, blank = True )
    image = ImageField( upload_to = 'images/', blank = True, verbose_name = 'Image' )
    latitude = models.DecimalField( max_digits = 12, decimal_places = 8, verbose_name = 'Latitude' )
    longitude = models.DecimalField( max_digits = 12, decimal_places = 8, verbose_name = 'Longitude' )
    url = models.URLField( blank = True, verbose_name = 'Url' )
    published = models.BooleanField( default = True, verbose_name = 'Published' )
    order = models.PositiveSmallIntegerField( verbose_name = 'Ordering', default = 0, help_text = '(le entry saranno ordinate dal numero più grande al più piccolo; si consiglia di utilizzare multipli di 10 per facilitare eventuali riordinamenti)' )
    
    def image_thumbnail(self, width = 300, height = 200):
        
        thumbnail_html = ''
        
        if self.image:
        
            try:
                thumbnail = get_thumbnail(self.image, '%sx%s' % (width, height, ))
                thumbnail_html = '<img src="%s" style="padding:5px;" height="%s" />' % (thumbnail.url, height, )
            
            except IOError:
                pass
        
        return thumbnail_html
    
    image_thumbnail.allow_tags = True
    image_thumbnail.short_description = image.verbose_name
    
    class Meta:
        
        ordering = ['-order', 'name']
        verbose_name = 'My Model'
        verbose_name_plural = 'My Models'
        
    def __unicode__(self):
        
        return unicode( self.name )
"""

        
"""
class SiteSettings( SingletonModel ):
    
    company = models.CharField( max_length = 100, blank = True, verbose_name = 'Ragione sociale' )
    piva = models.CharField( max_length = 50, blank = True, verbose_name = 'P.IVA' )
    copyright = models.CharField( max_length = 100, blank = True, verbose_name = 'Copyright' )
    
    phone = models.CharField( max_length = 50, blank = True, verbose_name = 'Telefono' )
    fax = models.CharField( max_length = 50, blank = True, verbose_name = 'Fax' )
    email = models.EmailField( blank = True, verbose_name = 'Email' )
    skype = models.CharField( max_length = 50, blank = True, verbose_name = 'Skype' )
    
    address = models.TextField( blank = True, verbose_name = 'Indirizzo' )
    latitude = models.DecimalField( max_digits = 12, decimal_places = 8, blank = True, null = True, verbose_name = 'Latitude' )
    longitude = models.DecimalField( max_digits = 12, decimal_places = 8, blank = True, null = True, verbose_name = 'Longitudine' )
    maps_url = models.URLField( blank = True, verbose_name = 'Maps URL' )
    
    google_analitycs_code = models.TextField( blank = True, verbose_name = 'Codice' )
    google_analitycs_id = models.CharField( max_length = 20, blank = True, verbose_name = 'ID' )
    
    facebook_url = models.URLField( blank = True, verbose_name = 'Facebook URL' )
    twitter_url = models.URLField( blank = True, verbose_name = 'Twitter URL' )
    linkedin_url = models.URLField( blank = True, verbose_name = 'LinkedIn URL' )
    youtube_url = models.URLField( blank = True, verbose_name = 'YouTube URL' )
    vimeo_url = models.URLField( blank = True, verbose_name = 'Vimeo URL' )
    googleplus_url = models.URLField( blank = True, verbose_name = 'Google+ URL' )
    tumblr_url = models.URLField( blank = True, verbose_name = 'Tumblr URL' )
    instagram_url = models.URLField( blank = True, verbose_name = 'Instagram URL' )
    pinterest_url = models.URLField( blank = True, verbose_name = 'Pinterest URL' )
    
    privacy_title = models.CharField( max_length = 50, blank = True, verbose_name = 'Titolo' )
    privacy_text = HTMLField( blank = True, verbose_name = 'Testo' )
    
    class Meta:
        
        app_label = 'Globali'
        verbose_name = 'Impostazioni [...]'
        verbose_name_plural = 'Impostazioni [...]'
        
    def __unicode__(self):
        
        return unicode( 'Impostazioni [...]' )
"""

