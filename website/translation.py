# -*- coding: utf-8 -*-

from django.conf import settings

from modeltranslation.translator import translator, TranslationOptions

from website.models import *


'''
class ModelTranslationOptions(TranslationOptions):
    fields = ('field1', 'field2', 'field3', )

translator.register(ModelTranslation, ModelTranslationOptions)
'''

'''
class SiteSettingsTranslationOptions( TranslationOptions ):
    
    fields = ('copyright', 'privacy_title', 'privacy_text', )
    

class SiteErrorsTranslationOptions( TranslationOptions ):
    
    fields = ('title_400', 'subtitle_400', 'title_403', 'subtitle_403', 'title_404', 'subtitle_404', 'title_500', 'subtitle_500', )

    
if settings.MULTI_LANGUAGE:
    
    translator.register( SiteSettings, SiteSettingsTranslationOptions )
    translator.register( SiteErrors, SiteErrorsTranslationOptions )
'''