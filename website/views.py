# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf import settings
# from django.core.exceptions import PermissionDenied
# from django.core.mail import mail_managers
# from django.contrib.auth import authenticate, login, logout
# from django.contrib.auth.decorators import login_required
# from django.contrib.auth.models import User
# from django.core.cache import cache
from django.http import (
    Http404, HttpResponse, HttpResponseBadRequest, HttpResponseForbidden, 
    HttpResponseNotFound, HttpResponseRedirect, HttpResponseServerError, 
)
from django.shortcuts import get_object_or_404, render
from django.template.loader import get_template

# from datetime import datetime, timedelta
# from dateutil.relativedelta import relativedelta

from navsy.urls import reverse

# from utls.http import FileResponse, PDFFileResponse, ZIPFileResponse, DynamicZIPFileResponse
# from utls.mail import mail
# from website.models import *

# import json
# import logging
# import requests
# import stripe


# def subdict(d, keys):
#     return { k:d[k] for k in keys }


def handler400(request):
    template = get_template('website/400.html')
    data = {}
    html = template.render(data, request)
    return HttpResponseBadRequest(html)


def handler403(request):
    template = get_template('website/403.html')
    data = {}
    html = template.render(data, request)
    return HttpResponseForbidden(html)


def handler404(request):
    template = get_template('website/404.html')
    data = {}
    html = template.render(data, request)
    return HttpResponseNotFound(html)


def handler500(request):
    template = get_template('website/500.html')
    data = {}
    html = template.render(data, request)
    return HttpResponseServerError(html)
    
    
def home(request):
    data = {}
    return render(request, 'website/home.html', data)
    
